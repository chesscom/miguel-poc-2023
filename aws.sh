#!/bin/bash
set -e
AWS_REGION="eu-north-1"

deploy_core() {
    aws cloudformation deploy \
        --region $AWS_REGION \
        --profile default \
        --template-file poc-core.yaml \
        --stack-name chesscom-poc \
        --capabilities CAPABILITY_IAM
}
TIMESTAMP=$(date +%s)
VERSION="chesscom-poc-${TIMESTAMP}"

deploy_new_version() {
    pushd src
    zip ../latest.zip ./*
    popd
    aws s3 cp latest.zip s3://chesscom-poc/ --profile default --region $AWS_REGION

    # update environment
}

create_version() {
    # create a new version
    aws elasticbeanstalk create-application-version \
        --application-name chesscom-poc \
        --version-label $VERSION \
        --source-bundle S3Bucket="chesscom-poc",S3Key="latest.zip" \
        --profile default \
        --region $AWS_REGION
}

update_version() {
    aws elasticbeanstalk update-environment \
        --application-name chesscom-poc \
        --environment-name EBEnvironmentEU \
        --version-label $VERSION \
        --region $AWS_REGION
}

deploy_core
# deploy_new_version
# create_version
# update_version