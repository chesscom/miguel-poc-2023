import { createClient } from 'redis'
import { WebSocketServer } from 'ws'

const client = createClient({
  // url: 'redis://localhost:6379'
})

client.connect().then(() => {
  console.log('[redis] connected')
  client.subscribe('event', message => {
    console.log('got event', message)
    broadcast(message)
  })
}).catch(err => {
  console.log('[redis] COULD NOT CONNECT', err)
})

const wss = new WebSocketServer({
  port: 8080,
  perMessageDeflate: {
    zlibDeflateOptions: {
      // See zlib defaults.
      chunkSize: 1024,
      memLevel: 7,
      level: 3
    },
    zlibInflateOptions: {
      chunkSize: 10 * 1024
    },
    // Other options settable:
    clientNoContextTakeover: true, // Defaults to negotiated value.
    serverNoContextTakeover: true, // Defaults to negotiated value.
    serverMaxWindowBits: 10, // Defaults to negotiated value.
    // Below options specified as default values.
    concurrencyLimit: 10, // Limits zlib concurrency for perf.
    threshold: 1024 // Size (in bytes) below which messages
    // should not be compressed if context takeover is disabled.
  }
});

function broadcast(message) {
  wss.clients.forEach(function each(client) {
    client.send(message, { binary: false})
  })
}