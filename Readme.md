## System Design
Briefly describe your proposed system design.

- It should be capable of scaling to 300 notifications/second average and 1000 notifications/second peak.
- Even if there are potentially better technologies available for this task, please constrain your system design to include only those technologies that you have implemented in a production environment.
- Include a link to your system diagram that shows all the deployable and configurable components of your design, eg services, load balancers, message queues, data stores, cache etc. (you can use https://docs.google.com/drawings); a link to the main sequence flow diagram (you can use https://www.websequencediagrams.com); and a sketch API design, including the shapes of any messages (you can use Protocol buffers or OpenAPI).

I must admit, when I started working on this task I assumed by "notifications" the question refered to push notifications, I have worked in systems using Firebase Cloud Messaging, but stressed over me not being the one who implemented that and how the task was worded. But since I can't see mention of "push" notifications I will continue on the assumption that the task refers to "in app" notifications, when the user is actively using the app.

<img src="chesscom.infra-01.png">

With this architecture there is a "Watcher system", which looks at the game and starts the notification flow when an event occurs, for example "Major piece captured". Keep in mind that this is based off AWS infrastructure, although the same things can be implemented in bare-metal off/own cloud infrastructure.
The watcher system publishes the event to the Redis Cluster master, which in turn replicates the message to all the other Redis replicas in each region (eu-central-1, us-east-1,... etc).
In each region there is a Beanstalk application subscribed to redis events (pub/sub), this beanstalk application implements a websocket server that distributes the message to all connected devices.
Route 53 implements Geo-location based routing, which guarantees that clients will connect to the nearest Elastic Beanstalk application.
The Elastic Beanstalk application implements auto-scaling based on websocket connections, the idea is that even though even a small EC2 instance can handle many active connections, service degradation is mitigated by spreading the load, so by leveraging Cloudwatch Alarms, and custom metrics, we can define how many websocket connections we want to have per EC2 instance